using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    GameObject player;         
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {            
            Vector3 p = player.transform.position;
            p.z = -10f;
            transform.position = p;
        }
        else
        {
            Debug.Log("We are looking for the player");
            player = GameObject.FindGameObjectWithTag("Player");
        }
        
    }
}
