using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField]
    float speed = 4;

    [SerializeField]
    float jump = 200;

    bool canJump = true;
    Vector3 startPosition;


    [SerializeField]
    GameObject myBullet;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position.z = Camera.main.transform.position.z;
        Camera.main.transform.position = position;
        

        // Get the value of the Horizontal axis base on pressed button
        // if the user press right => value will be positive
        // if the user press left => value will be negative
        float value = Input.GetAxis("Horizontal");
        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);

        if(value > 0){
            //Position
            //Rotation
            //Scale (x, y, z)
            //       +
            transform.localScale = new Vector3(1, 1, 1);
        }else if(value < 0){
            //       -
            transform.localScale = new Vector3(-1, 1, 1);
        }


        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            body.AddForce(new Vector2(0, jump));
            canJump = false;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bullet = Instantiate(myBullet, transform.position, Quaternion.identity);
            Bullet150 bulletScript = bullet.GetComponent<Bullet150>();
            bulletScript.direction = transform.localScale.x;

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.gameObject.tag == "EnemyPlayer")
        {
            transform.position = startPosition;
            return;
        }

        Vector2 point = collision.contacts[0].normal;
        if (point.y > 0)
        {
            canJump = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "bullet")
        {
            return;
        }


        if(collision.gameObject.tag == "CheckPoint")
        {
            SpriteRenderer sr = collision.gameObject.GetComponent<SpriteRenderer>();
            sr.color = Color.green;

            startPosition = collision.gameObject.transform.position;
            return;
        }

        GameObject eventSystem = GameObject.Find("EventSystem");
        EventSystemScript ess = eventSystem.GetComponent<EventSystemScript>();
        

        GameObject other = collision.gameObject;
        TheCoin code = other.GetComponent<TheCoin>();
        if(other.tag == "AffectScore") {
            ess.AddScore((int)code.value);
         } else if (other.tag == "AffectHealth"){
            if(code.value > 0 && ess.GetHealth() >= 100)
            {
                return;
            }
            
            //if (code.value < 0) {
            //    return;
            //}

            ess.AddHealth(code.value);

        }

        Destroy(other);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        //GameObject eventSystem = GameObject.Find("EventSystem");
        //EventSystemScript ess = eventSystem.GetComponent<EventSystemScript>();


        //GameObject other = collision.gameObject;
        //TheCoin code = other.GetComponent<TheCoin>();
        //if (other.tag == "AffectHealth")
        //{
            //if (code.value < 0)
            //{
                //ess.AddHealth(code.value);
            //}
        //}

    }
}
