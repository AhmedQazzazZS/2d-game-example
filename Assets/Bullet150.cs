using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet150 : MonoBehaviour
{

    int lifeTime = 90;
    public float direction = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(12, 0) * Time.deltaTime * direction);
        lifeTime -= 1;
        if(lifeTime == 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag != "Player") {
            Destroy(gameObject);
        }
        
    }
}
