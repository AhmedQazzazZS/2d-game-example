using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelController150 : MonoBehaviour
{

    int score = 0;
    int health = 100;

    [SerializeField]
    Text scoreText;

    [SerializeField]
    Text healthText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetHealth()
    {
        return health;
    }

    public void AddScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score;
    }

    public void AddHealth(int value)
    {
        health += value;
        health = Mathf.Min(health, 100);
        healthText.text = "Health: " + health;
    }
}
