using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEvents : MonoBehaviour
{

    int score = 0;

    [SerializeField]
    Text txt_score;

    float health = 100;

    [SerializeField]
    Text txt_health;

    public float GetHealth()
    {
        return health;
    }

    public void addScore(int value)
    {
        score += value;
        txt_score.text = "Score: " + score;
    }

    public void addHealth(float value)
    {
        health += value;
        txt_health.text = "Health: " + (int)health;
    }


    public void ResetHealth()
    {
        health = 100;
        txt_health.text = "Health: " + (int)health;
    }
}
