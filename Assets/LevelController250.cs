using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController250 : MonoBehaviour
{

    int score = 0;

    [SerializeField]
    Text scoreText;

    int health = 100;

    [SerializeField]
    Text healthText;

    public void AddScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score;
    }

    public void AddHealth(int value)
    {
        health += value;
        health = Mathf.Min(100, health);
        healthText.text = "Health: " + health;
    }

    public int GetHealth()
    {
        return health;
    }
}
