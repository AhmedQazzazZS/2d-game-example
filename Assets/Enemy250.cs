using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy250 : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = 5;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(speed, 0) * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision){
        speed *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision){
        GameObject other = collision.gameObject;
        if(other.tag == "bullet"){
            Destroy(other);
            Destroy(gameObject);
        }
    }

}
