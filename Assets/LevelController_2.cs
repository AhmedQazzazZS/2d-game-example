using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController_2 : MonoBehaviour
{

    int score = 0;
    int health = 100;

    [SerializeField]
    Text scroeText;

    [SerializeField]
    Text healthText;

    public int getHealth()
    {
        return health;
    }


    public void AddScore(int value)
    {
        score += value;
        scroeText.text = "Score: " + score;
    }

    public void AddHealth(int value)
    {
        health += value;
        healthText.text = "Health: " + health;
    }
}
