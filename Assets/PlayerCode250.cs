using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCode250 : MonoBehaviour
{
    [SerializeField]
    float speed = 3;

    [SerializeField]
    float jump = 100;

    Vector3 startPosition;

    [SerializeField]
    GameObject bulletPrefab;

    bool canJump = true;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        Vector3 position = Vector3.MoveTowards(Camera.main.transform.position, transform.position, 0.5f);
        position.z = -10;
        Camera.main.transform.position = position;

        //Input.GetKey(KeyCode.LeftAlt);
        float value = Input.GetAxis("Horizontal") ;
        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);

        if(value > 0) {
            transform.localScale = new Vector3(1, 1, 1);
        }else if (value < 0 ){
            transform.localScale = new Vector3(-1, 1, 1);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            body.AddForce(new Vector2(0, jump));
            canJump = false;
        }

        if(Input.GetKeyDown(KeyCode.Space)) {
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            Bullet250 bulletCode = obj.GetComponent<Bullet250>();
            bulletCode.direction = transform.localScale.x;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject other = collision.collider.gameObject;
        if (other.tag == "EnemyPlayer"){
            transform.position = startPosition;
            Vector3 position = startPosition;
            position.z = -10;
            Camera.main.transform.position = position;
            return;
        }
        
        Vector2 point = collision.contacts[0].normal;
        if(point.y > 0)
        {
            canJump = true;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;

        if(other.tag == "bullet") {return;}

        ObjectValue250 objv = other.GetComponent<ObjectValue250>();
        

        GameObject eventSystem = GameObject.Find("EventSystem");
        LevelController250 lvc = eventSystem.GetComponent<LevelController250>();
        if (other.tag == "AffectScore")
        {
            lvc.AddScore(objv.value);
        }
        else if (other.tag == "AffectHealth")
        {

            if(objv.value > 0)
            {
                if (lvc.GetHealth() == 100) {
                    return;
                }
                int difference = 100 - lvc.GetHealth();
                if (objv.value > difference) {
                    lvc.AddHealth(difference);
                    objv.value -= difference;
                    return;
                }
            }


            lvc.AddHealth(objv.value);
        }

        Destroy(other);
    }
}
