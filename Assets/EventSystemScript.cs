using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventSystemScript : MonoBehaviour
{


    int score = 0;
    float health = 100;

    [SerializeField]
    Text txtScore;

    [SerializeField]
    Text txtHealth;

    public float GetHealth()
    {
        return health;
    }

    public void AddHealth(float value)
    {
        health += value;
        txtHealth.text = "Health: " + (int)health;
    }


    public void AddScore(int value)
    {
        score += value;
        txtScore.text = "Score: " + score;
    }
}
