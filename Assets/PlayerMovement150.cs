using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement150 : MonoBehaviour
{

    [SerializeField]
    float speed = 3;

    [SerializeField]
    float jump = 100;

    bool canJump = true;
    Vector3 startPosition;
    // Start is called before the first frame update

    [SerializeField]
    GameObject bulletPrefab;

    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        Vector3 position = Vector3.MoveTowards(Camera.main.transform.position, transform.position, 0.6f);
        position.z = -10;
        Camera.main.transform.position = position;

        float value = Input.GetAxis("Horizontal");
        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);
        if(value > 0 ){
            transform.localScale = new Vector3(1, 1, 1);
        }else if (value < 0){
            transform.localScale = new Vector3(-1, 1, 1);
        }


        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            body.AddForce(new Vector2(0, jump));
            canJump = false;
        }

        if(Input.GetKeyDown(KeyCode.Space)){
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            bullet150ForReal code = obj.GetComponent<bullet150ForReal>();
            code.direction = transform.localScale.x;
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject other = collision.collider.gameObject;
        if(other.tag == "EnemyPlayer"){
           transform.position = startPosition;
           Vector3 newCameraPosition = startPosition;
            newCameraPosition.z = Camera.main.transform.position.z;
            Camera.main.transform.position = newCameraPosition;
           return;
        }
        
        
        Vector2 point = collision.contacts[0].normal;

        if (point.y > 0)
        {
            canJump = true;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if(other.tag == "bullet") { return ;}


        ObjectValue objV = other.GetComponent<ObjectValue>();
        

        GameObject eventSystem = GameObject.Find("EventSystem");
        levelController150 lvc = eventSystem.GetComponent<levelController150>();
        if(other.tag == "AffectScore")
        {
            lvc.AddScore(objV.value);
        }
        else if (other.tag == "AffectHealth")
        {

            //if ((lvc.GetHealth() < 100 && objV.value > 0) || objV.value < 0)
            //{
            //    lvc.AddHealth(objV.value);
            //}

            if(lvc.GetHealth() == 100 && objV.value > 0)
            {
                return;
            }

            lvc.AddHealth(objV.value);
        }
        
        Destroy(other);
    }
}
