using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat201 : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject player;
    Vector3 startPosition;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(startPosition, player.transform.position);
        if(distance < 15)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 0.02f);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 0.02f);
        }

    }
}
