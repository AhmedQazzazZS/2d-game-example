using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCode : MonoBehaviour
{

    [SerializeField]
    float speed = 4;

    [SerializeField]
    float jumpForce = 300;
    

    bool canJump = true;

    Vector3 startPosition;
    // Start is called before the first frame update

    [SerializeField]
    GameObject bulletPrefab;

    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 position = transform.position;
        position.z = Camera.main.transform.position.z;
        Camera.main.transform.position = position;

        float value = Input.GetAxis("Horizontal");
        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);

        if (value > 0 ) { transform.localScale = new Vector3(1, 1, 1); }
        else if (value < 0) { transform.localScale = new Vector3(-1, 1, 1); }

        if (Input.GetKeyDown(KeyCode.UpArrow) &&
            canJump)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            canJump = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            Bullet101 b = obj.GetComponent<Bullet101>();
            b.direction = transform.localScale.x;

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        GameObject other = collision.collider.gameObject;
        if(other.tag == "EnemyPlayer")
        {
            transform.position = startPosition;
            return;
        }        

        Vector2 normal = collision.contacts[0].normal;
        if(normal.y > 0)
        {
            canJump = true;
        }
    }
   

    private void OnTriggerEnter2D(Collider2D collision)
    {

        GameObject other = collision.gameObject;
        CollectableScript cs = other.GetComponent<CollectableScript>();

        GameObject eventSystem = GameObject.Find("EventSystem");
        LevelController_2 lvc = eventSystem.GetComponent<LevelController_2>();
        if (other.tag == "AffectScore") {
            lvc.AddScore(cs.value);
            Destroy(other);
        } else if (other.tag == "AffectHealth"){
            if(cs.value > 0 && lvc.getHealth() < 100)
            {
                lvc.AddHealth(cs.value);
                Destroy(other);
            }else if(cs.value < 0)
            {
                lvc.AddHealth(cs.value);
                Destroy(other);
            }
        }        

    }



}
