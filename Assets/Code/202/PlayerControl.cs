using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    [SerializeField]
    float speed = 4;

    [SerializeField]
    float jumpForce = 100;

    bool canJump = true;

    Vector2 startPoint;
    int respawnCount = 0;
    bool shouldRespawn = false;

    [SerializeField]
    GameObject bulletPrefab;

    bool playerInDanger = false;

    UIEvents eventSystem;

    // Start is called before the first frame update
    void Start()
    {
        startPoint = transform.position;
        GameObject obj = GameObject.Find("EventSystem");
        eventSystem = obj.GetComponent<UIEvents>();
    }



    public void PointerDownLeft()
    {
        transform.Translate(new Vector2(-1, 0) * Time.deltaTime * speed);
    }

    public void PointerDownRight()
    {
        transform.Translate(new Vector2(1, 0) * Time.deltaTime * speed);
    }


    // Update is called once per frame
    void Update()
    {

        if(eventSystem.GetHealth() <= 0)
        {
            eventSystem.ResetHealth();
            transform.position = startPoint;
        }

        if (playerInDanger)
        {
            eventSystem.addHealth(-0.2f);
        }

        Vector3 playerPosition = transform.position;
            playerPosition.z = -10;
        Camera.main.transform.position = playerPosition;

        if (shouldRespawn)
        {
            respawnCount += 1;
            if (respawnCount == 90)
            {
                transform.position = startPoint;
                shouldRespawn = false;
            }
        }

        float value = Input.GetAxis("Horizontal");
        if (value < 0) { transform.localScale = new Vector3(-1, 1, 1); }
        else if (value > 0) { transform.localScale = new Vector3(1, 1, 1);}


        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);

        //GameObject cam = GameObject.Find("Main Camera");
        //Camera camComponent = cam.GetComponent<Camera>();
        //camComponent.orthographicSize =  5 + Mathf.Abs(value);


        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            body.AddForce(new Vector2(0, jumpForce));
            canJump = false;
        }


        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            Bullet202 bulletCode = obj.GetComponent<Bullet202>();
            bulletCode.direction = transform.localScale.x;
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        GameObject other = collision.collider.gameObject;
        if(other.tag == "EnemyPlayer")
        {
            transform.position = startPoint;
            return;
        }

       

        Vector2 normal =  collision.contacts[0].normal;

        if (normal.y > 0)
        {
            canJump = true;
        }
    }


    //private void OnTriggerStay2D(Collider2D collision)
    //{
    //    GameObject other = collision.gameObject;

    //    if (other.tag == "EnemyPlayer")
    //    {
    //        GameObject obj = GameObject.Find("EventSystem");
    //        UIEvents evnts = obj.GetComponent<UIEvents>();
    //        evnts.addHealth(-0.2f);
    //    }

    //}

    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;

        if (other.tag == "EnemyPlayer")
        {
            playerInDanger = false;
            return;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        GameObject other = collision.gameObject;

        if (other.tag == "EnemyPlayer")
        {
            //    GameObject obj = GameObject.Find("EventSystem");
            //    UIEvents evnts = obj.GetComponent<UIEvents>();
            //    evnts.addHealth(-2);
            playerInDanger = true;
            return;
        }

        if (other.tag == "CheckPoint")
        {

            CheckPoint cp = other.GetComponent<CheckPoint>();
            if (cp.isChecked == false)
            {
                startPoint = other.transform.position;
                SpriteRenderer sr = other.GetComponent<SpriteRenderer>();
                sr.color = Color.green;
                cp.isChecked = true;
            }

            return;
        }


        if (collision.gameObject.name == "DeathLine")
        {
            //Destory this object.

            //Create new version.

            //OR Jump to start point.
            shouldRespawn = true;
            return;
        }

        
        int v = other.GetComponent<CoinScript>().value;        
        GameObject eventSystem = GameObject.Find("EventSystem");
        UIEvents events = eventSystem.GetComponent<UIEvents>();
        if (other.tag == "AffectScore")
        {
            events.addScore(v);
        }
        else if(other.tag == "AffectHealth")
        {
            if(v > 0)
            {
                if (events.GetHealth() >= 100)
                {
                    return;
                }
            }
            events.addHealth((float)v);
        }

        Destroy(collision.gameObject);

    }


}
