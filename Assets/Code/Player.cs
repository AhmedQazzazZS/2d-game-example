using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    GameObject camera;

    public float playerSpeed = 4;
    public float jumpPower = 12;
    bool canJump = true;

    bool isMovingRight = false;
    bool isMovingLeft = false;

    GameObject bulletPrefab;

    void Start()
    {
        bulletPrefab = Resources.Load<GameObject>("Bullet");
    }



    public void MovePlayerRightPointerDown()
    {
        isMovingRight = true;
    }

    public void MovePlayerRightPointerUp()
    {
        isMovingRight = false;
    }

    public void MovePlayerLeftPointerDown()
    {
        isMovingLeft = true;
    }

    public void MovePlayerLeftPointerUp()
    {
        isMovingLeft = false;
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            Vector3 tPoint = Camera.main.ScreenToWorldPoint(t.position);
            tPoint.z = 0f;
            transform.position = tPoint;
        }


        float HAxes = 0f;

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.Android)
        {
            if (isMovingRight) { HAxes = 1f; }
            else if (isMovingLeft) { HAxes = -1f; }
            else { HAxes = 0f; }
        }
        else {
            HAxes = Input.GetAxis("Horizontal");
        }


        if (HAxes > 0) { transform.localScale = new Vector3(1, 1, 1); }
        if (HAxes < 0) { transform.localScale = new Vector3(-1, 1, 1); }


        transform.Translate(new Vector2(HAxes, 0) * Time.deltaTime * playerSpeed);

        //Camera cObj = camera.GetComponent<Camera>();
        //cObj.orthographicSize = 8.676287f + (Mathf.Abs(HAxes) * 0.5f);


        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower));
            canJump = false;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject o = Instantiate(bulletPrefab,transform.position, Quaternion.identity);
            if (transform.localScale.x < 0)
            {
                o.GetComponent<BulletCode>().SetMovingDirection(BulletMovingDirection.left);
            }
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        Collider2D c = collision.otherCollider;

        var normal = collision.contacts[0].normal;
        if (normal.y > 0)
        {
            canJump = true;
        }

        Debug.Log(normal.x);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        UIManager.SharedManager().AddScore(1);
    }





}
