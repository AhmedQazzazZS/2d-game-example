using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    Button rightButton;

    [SerializeField]
    Button leftButton;

    [SerializeField]
    RectTransform GameOverPanel;

    [SerializeField]
    private static UIManager shared;
    int score = 0;

    [SerializeField]
    Text scoreText;

    [SerializeField]
    Image barValueImage;

    float healthValue = 100;
    bool showGameOver = false;

    public static UIManager SharedManager()
    {
        return UIManager.shared;
    }

    // Start is called before the first frame update
    void Start()
    {
        UIManager.shared = GetComponent<UIManager>();
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.Android)
        {
            rightButton.gameObject.active = true;
            leftButton.gameObject.active = true;
        }
        else
        {
            Destroy(rightButton.gameObject);
            Destroy(leftButton.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        healthValue -= 0.1f * Time.deltaTime;
        RectTransform originalBarTransform = (RectTransform)barValueImage.transform.parent;

        float newWidth = originalBarTransform.rect.width * (healthValue / 100);
        //Rect r = originalBarTransform.rect;
        //r.width = newWidth
        //originalBarTransform.rect = r;

        barValueImage.rectTransform.sizeDelta = new Vector2(newWidth, originalBarTransform.rect.height);
        barValueImage.color = Color.Lerp(Color.red, Color.green, (float)((float)healthValue / (float)100));
        if(healthValue <= 0)
        {
            
            if (showGameOver == false)
            {
                healthValue = 0f;
                Time.timeScale = 0f;
                showGameOver = true;
                GameOverPanel.gameObject.active = true;
                GameObject gameover = GameObject.Find("GameOver");
                gameover.GetComponent<Animator>().SetBool("animate", true);
            }
            
        }

    }

    public void AddScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score;
        
    }

}
