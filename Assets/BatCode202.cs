using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatCode202 : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject player;

    Vector3 startPosition;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float d = Vector3.Distance(startPosition, player.transform.position);
        if (d < 10)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 4 * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 4 * Time.deltaTime);
        }
    }
}
