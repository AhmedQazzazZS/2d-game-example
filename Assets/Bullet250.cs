using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet250 : MonoBehaviour
{
    float speed = 10;
    public float direction = 1;
    int lifeTime = 5 * 60;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(speed * direction, 0) * Time.deltaTime);
        lifeTime -= 1;
        if(lifeTime == 0){
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision){
        GameObject other = collision.gameObject;
        if(other.tag == "Solid"){
            Destroy(gameObject);
        }
    }
}
