using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_1 : MonoBehaviour
{

    [SerializeField]
    float movementSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(movementSpeed, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 point = collision.contacts[0].normal;
        Debug.Log(point);
        if (point.x != 0)
        {
            movementSpeed *= -1;
        }
    }

 
}
