using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy201 : MonoBehaviour
{

    [SerializeField]
    int movement = 1;
    // Start is called before the first frame update


    GameObject ScorePrefab;
    void Start()
    {
        ScorePrefab = Resources.Load<GameObject>("score");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(movement, 0) * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        movement *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if(other.tag == "bullet")
        {
            Instantiate(ScorePrefab, transform.position, Quaternion.identity);
            Destroy(other);
            Destroy(gameObject);
        }
    }
}
