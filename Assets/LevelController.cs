using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{

    [SerializeField]
    Text txtScore;

    [SerializeField]
    Text txtHealth;


    int score = 0;
    int health = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddScore(int value)
    {
        score += value;
        txtScore.text = "Score: " + score;
    }

    public void AddHealth(int value)
    {
        health += value;
        txtHealth.text = "Health: " + health;
    }
}
