using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy101 : MonoBehaviour
{
    [SerializeField]
    float speed = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(speed, 0) * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        speed *= -1;
    }

}
