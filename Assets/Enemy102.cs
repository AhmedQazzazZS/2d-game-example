using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy102 : MonoBehaviour
{

    float movingDirection = 1;

    [SerializeField]
    float speed = 4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(movingDirection, 0) * Time.deltaTime * speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        movingDirection *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            Destroy(gameObject);
        }
    }

}
