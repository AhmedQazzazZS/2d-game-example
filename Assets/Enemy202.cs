using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy202 : MonoBehaviour
{

    float direction = 1;

    [SerializeField]
    float speed = 4;

    [SerializeField]
    int live = 1;

    GameObject scorePrefab;

    // Start is called before the first frame update
    void Start()
    {
        scorePrefab = Resources.Load<GameObject>("score");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(direction, 0) * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(direction);
        direction *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if(other.tag == "bullet")
        {
            live -= 1;
            Destroy(other);
            if (live == 0)
            {
                Vector2 position = transform.position;
                position.y += 1;

                GameObject obj = Instantiate(scorePrefab, position, Quaternion.identity);
                Destroy(gameObject);
            }            
        }
    }

}
