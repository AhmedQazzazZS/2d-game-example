using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet201 : MonoBehaviour
{

    int lifeTime = 100;

    public float direction = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(12 * direction, 0) *  Time.deltaTime);
        lifeTime -= 1;
        if(lifeTime == 0)
        {
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Solid")
        {
            Destroy(gameObject);
        }
        
    }
}
