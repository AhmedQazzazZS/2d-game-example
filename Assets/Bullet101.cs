using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet101 : MonoBehaviour
{

    int lifeTime = 300;
    public float direction = 1;
    public float speed = 10;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(5 * direction, 0) * Time.deltaTime * speed);
        lifeTime -= 1;
        if(lifeTime == 0)
        {
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision){
        GameObject other = collision.gameObject;
        if(other.tag == "Solid") {
            Destroy(gameObject);
        }else if (other.tag == "EnemyPlayer"){
             Destroy(other);
            Destroy(gameObject);
        }
    }
    
    // private void OnCollisionEnter2D(Collision2D collision)
    // {
    //     GameObject other = collision.collider.gameObject;
    //     if(other.tag == "EnemyPlayer")
    //     {
    //         Destroy(other);
    //         Destroy(gameObject);
    //     }
    // }
}
