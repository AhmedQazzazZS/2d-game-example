using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{


    [SerializeField]
    float speed = 1;

    [SerializeField]
    float jumpForce = 200;

    bool canJump = true;

    Vector3 startPosition;


    [SerializeField]
    GameObject bulletPrefab;

    GameObject lastCheckPoint;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerPosition = Vector3.MoveTowards(Camera.main.transform.position, transform.position, 0.5f);
        playerPosition.z = Camera.main.transform.position.z;
        Camera.main.transform.position = playerPosition;

        float value = Input.GetAxis("Horizontal");
        transform.Translate(new Vector2(value, 0) * Time.deltaTime * speed);

        if (value > 0) { transform.localScale = new Vector3(1, 1, 1); }
        else if (value < 0) { transform.localScale = new Vector3(-1, 1, 1); }


        if (Input.GetKeyDown(KeyCode.UpArrow) && canJump)
        {
            Rigidbody2D body = GetComponent<Rigidbody2D>();
            body.AddForce(new Vector2(0, jumpForce));
            canJump = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            bullet201 bulletCode = obj.GetComponent<bullet201>();
            bulletCode.direction = transform.localScale.x;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       

        if (collision.collider.gameObject.tag == "EnemyPlayer")
        {
            transform.position = startPosition;
            return;
        }
        

        Vector2 point = collision.contacts[0].normal;
        if (point.y > 0) {
            canJump = true;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;

        if (other.tag == "bullet") { return; }

        if(other.tag == "CheckPoint")
        {
            //CheckPoint201 cp = other.GetComponent<CheckPoint201>();
            //if (cp.isChecked == false){
                startPosition = other.transform.position;

                SpriteRenderer sr = other.GetComponent<SpriteRenderer>();
                sr.color = Color.green;
               // cp.isChecked = true;
            //}

            if (lastCheckPoint != null && lastCheckPoint != other)
            {
                SpriteRenderer lsr = lastCheckPoint.GetComponent<SpriteRenderer>();
                lsr.color = Color.blue;
            }
            lastCheckPoint = other;

            return;
        }

        GameObject ev = GameObject.Find("EventSystem");
        LevelController lc = ev.GetComponent<LevelController>();
        
        CoinValue cv = other.GetComponent<CoinValue>();

        if(other.tag == "AffectScore")
        {
            lc.AddScore(cv.value);
        }else if(other.tag == "AffectHealth")
        {
            lc.AddHealth(cv.value);
        }
        
        Destroy(other);
    }
}
