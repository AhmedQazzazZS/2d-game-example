using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCode : MonoBehaviour
{

    int direction = 1;
    // Start is called before the first frame update

    int liveTime = 60;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector2(direction, 0) * 10 * Time.deltaTime);
        liveTime -= 1;
        if (liveTime == 0)
        {
            Destroy(gameObject);
        }
    }

    public void SetMovingDirection(BulletMovingDirection dir)
    {
        if (dir == BulletMovingDirection.left)
        {
            direction = -1;
        }
    }
}


public enum BulletMovingDirection
{
    left,
    right
}